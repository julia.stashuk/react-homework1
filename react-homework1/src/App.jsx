
import './App.css'

import React from 'react';
import Button from './Component/Button/Button';
import Modal from './Component/Modal/Modal';

class App extends React.Component {
    state = {
      causedModal: null,
    };
  
    firstModalActions = [
      <Button
        className="main-button"
        backgroundColor="#8A33FD"
        key="cnsbtn"
        func={() =>
          this.setState((state) => ({
            ...state,
            causedModal: null,
          }))
        }
        text="NO, CANCEL"
        color="white"
      />,
      <Button
        className="main-button button-text-color"
        backgroundColor="white"
        key="okbtn"
        text="YES, DELETE"
        func={() => alert("OK!")}
      />
    ];
    secondModalActions = [
      <Button
        className="main-button"
        backgroundColor="#8A33FD"
        key="okbtn"
        text="ADD TO FAVORITE"
        func={() => alert("CONFIRMED!")}
      />
    ];
  
    closeBtn = () => {
      this.setState(
        (state) =>
          (state = {
            ...state,
            causedModal: null,
          })
      );
    };
  
    handleClick = (e) => {
      return this.setState(
        (state) =>
          (state = {
            ...state,
            causedModal: e.target.id
          })
      );
    };
  
    render() {
      const { causedModal } = this.state;

      return (
        <div className="App">
          <div className="titleButtons">
           
            <Button
              id="opnfirstmod"
              className="main-button"
              func={this.handleClick}
              backgroundColor="green"
              text="Open first modal"
            />
           
            <Button
              id="opnsecmod"
              className="main-button"
              func={this.handleClick}
              backgroundColor="blue"
              text="Open second modal"
            />
          </div>

         
          {causedModal === "opnfirstmod" ? (
            <Modal 
              backgroundColor="white"
              closeButton={true}
              showImage={true}
              func={this.closeBtn}
              header={"Product Delete!"}
              text={
                <p className="modal-text">
                  By clicking the "Yes,Delete" button, PRODUCT NAME will be deleted.
                </p>
              }
              actions={this.firstModalActions}
            />
          ) : null}
          {causedModal === 'opnsecmod' ? (
            <Modal
              backgroundColor="white"
              closeButton={true}
              showImage={false}
              func={this.closeBtn}
              header={'Add Product "NAME"'}
              text={
                <p className="modal-text">
                  Description for you product
                </p>
              }
              actions={this.secondModalActions} 
            />
          ) : null}
        </div>
      );
    }
  }
  
  export default App;
  
  
