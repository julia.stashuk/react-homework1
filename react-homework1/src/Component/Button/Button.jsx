import React from 'react';
import './Button.scss';

class Button extends React.Component {
    render() {
        const {id, className, text, backgroundColor, func} =this.props
        return (
          <button id = {id} className = {className} style = {{backgroundColor}} onClick = {func}>
            {text}
          </button>
        );
      }
    }
    

export default Button;
