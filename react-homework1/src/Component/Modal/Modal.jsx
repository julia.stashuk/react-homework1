import React from 'react';
import './Modal.scss';

class Modal extends React.Component {
    render() {
        const {header, text, func, backgroundColor, actions, closeButton, showImage} = this.props;
        
        function handleClick(e) {
          if (e.target.className === "modal-wrapper") {
            func();
          }
        }
        return (
          <div className = "modal-wrapper" onClick = {handleClick}>
            <div className = "modal" style = {{ backgroundColor }}>
              <div className = "modal-box">
              {!!closeButton ? <p className = 'modal-close' onClick = {func} ></p>:null}
              <div className = "modal-image">
              {showImage && (<svg width="276" height="140" viewBox="0 0 276 140" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="276" height="140" fill="#D9D9D9"/>
</svg>)}
</div>
              <div className = 'modal-header'>
                <h1 className = "modal-title">{header}</h1>
                </div>
              <div className = "modal-content">
                <p>{text}</p>
              </div>
              <div className = "modal-footer">
                {actions}
              </div>
            </div>
          </div>
          </div>
        );
      }
    }

export default Modal;